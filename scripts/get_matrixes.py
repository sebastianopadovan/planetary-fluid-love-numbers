import numpy as np
from constants import *

def fundamental_matrix(n):
   G = np.zeros((2,2))
   G[0,0] = 1.
   G[0,1] = 1.
   G[1,0] = n
   G[1,1] = -(n+1.)
   detG   = -(2*n+1)
   Gp = np.linalg.inv(G)
   return G, Gp 

def core_propagator_matrix(n,r_core):
   C = np.zeros((2,2))
   C[0,0] = r_core**n
   C[0,1] = r_core**(-(n+1))
   C[1,0] = n*r_core**n
   C[1,1] = -(n+1.)*r_core**(-(n+1))
   return C

def boundary_matrix(g,del_rho,r):
   B = np.identity(2)
   B[1,0] = -4*np.pi*grav_const*del_rho*r/g
   return B

def radial_matrix(n,r):
   x = np.diag(np.array([r**n, r**(-(n+1))]))
   return x

   
