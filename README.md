#Purpose#

The three python scripts provided



```python
kn_calc_functions.py
get_matrixes.py
constants.py
```

can be used to compute the fluid Love number *k* for a planet given its density profile.
The details of the calculation can be found in the publication described at the bottom of this file.


---

## Usage

In your python script or notebook insert the following line:
 

```python
import kn_calc_functions as knf
```

Given your spherically symmetric planetary interior model, 
you can obtain the Love number *k* of order *n* with the following call:


```python
kn = knf.calc_kn(n,r,d)
```
where *n* is the degree of the Love number, *r* is the array with the radius, and *d* the density.
Note that:

1.	The planet interior model is made up of a series of concentric spherical shell. Each shell has a constant density;
2.	The radial array contains the *outer* boundary of each shell, starting with the central shell. Thus, 
	the first entry is the radius of the central shell, the last the radius of the planet 
	(the radial array, thus, _must not start with 0_);
3.	Similarly, the density array is ordered from the center to the surface;
4.	The degree *n* is an integer larger or equal than 2;
5.	The value of *k* for *n=2* must be between 0 and 1.5 for gravitationally stable planets, where density 
	decreases with radius.

---

## Examples

The folder scripts/ contains a python notebook with two worked examples:

1.	The computation of the Love numbers *k_n* for a homogeneous planet;
2.	The retrieval of the PREM data and the computation of *k_2* for the Earth.

From a terminal in the folder where you dowload the notebook and the scripts, run the command 


```shell
> jupyter notebook
```

Basic workflow in a jupyter notebook can be found, e.g., here:
https://jupyter-notebook-beginner-guide.readthedocs.io/en/latest/execute.html#executing-a-notebook

---

##Related publication##

Padovan S., T. Spohn, P. Baumeister, N. Tosi, D. Breuer, Sz. Csizmadia, H. Hellard, and F. Sohl,
Matrix-propagator approach to compute fluid Love numbers and applicability 
to extrasolar planets, *Astronomy & Astrophysics*, 2018. doi:10.1051/0004-6361/201834181 
(https://doi.org/10.1051/0004-6361/201834181)

Also available on ArXiv: https://arxiv.org/abs/1810.10064

###Abstract###

 *Context.*
   The mass and radius of a planet directly provide its 
   bulk density, which can be interpreted in terms of its 
   overall composition.
   Any measure of the radial mass distribution provides 
   a first step in
   constraining the interior structure.
   The fluid Love number *k_2* provides such a measure, 
   and estimates of *k_2* for extrasolar planets are expected 
   to be available in the coming years thanks to improved 
   observational facilities 
   and the ever-extending temporal baseline of 
   extrasolar planet observations.    
   
*Aims.*
   We derive a method for calculating the Love numbers *k_n* of any 
   object given its density profile, which is routinely 
   calculated from 
   interior structure codes.   
   
*Methods.*
   We used the matrix-propagator technique, a method frequently 
   used in the geophysical community.   
   
*Results.*
   We detail the calculation and apply it to the case of GJ 436b, a classical example of the degeneracy of mass-radius relationships,
   to illustrate how measurements of *k_2* can improve our understanding of the interior structure of extrasolar planets.
   We implemented the method in a code that is fast, freely available, and easy to combine with preexisting
   interior structure codes. While the linear approach presented here for the calculation of the Love numbers 
   cannot treat the presence of nonlinear effects that may arise under certain dynamical conditions,
   it is applicable to close-in gaseous extrasolar planets like hot Jupiters, likely the first targets for which *k_2* will be measured.


